'use strict';

angular.module('controllers')
.controller('DirectionsCtrl', ['$scope', '$http',
        function ($scope, $http) {
            $http.get('/app/components/directions/directions.json')
                .success(function (hallDirectionMapping) {
                    $scope.hallDirectionMapping = hallDirectionMapping;
                });
        }]);
