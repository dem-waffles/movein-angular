'use strict';

angular.module('controllers')
.controller('ChecklistCtrl', ['$scope',
        function ($scope) {

            var RSL_LOCALSTORAGE_KEY = 'csm-rsl-movein-3';

            var starterChecklist = [
            {
                id: '8296FCC8-8517-4373-B25A-3ADA8FE0A6DC',
                text: 'Prescription Medications and things your student might need like Tylenol, ibuprofen, tums, vitamins etc.',
                checked: false
            },
            {
                id: '21BAA985-E904-4877-8AE0-437C57277514',
                text: 'Small first aid kit & Ice Pack',
                checked: false
            },
            {
                id: 'EAE207A6-9C96-41CB-91F3-6A2797F4FA8C',
                text: 'U-Lock for your bicycle – Chain and cable locks are not recommended by Mines Police Dept.',
                checked: false
            },
            {
                id: '48D56C7D-BBB5-46F6-A6EA-335F56399774',
                text: '10 Pound Rock – It is more meaningful if it is from home',
                checked: false
            },
            {
                text: 'Pool Noodle cut the long way to the center. This can be used on bottom bunks to avoid hitting your head on steel.',
                id: 'B01568EE-FAB9-4DE1-8D4E-A813F57B6078',
                checked: false
            },
            {
                id: 'DEFD6756-B834-4F1A-A22B-D6C87A9F6B38',
                text: 'Sheets (Twin XL – if you can’t bring this to campus, there are many stores nearby and the Bed, Bath and Beyond near campus offers discounts just for Mines students!), Blanket, pillow',
                checked: false
            },
            {
                id: '3BC01C3B-9729-4C8E-981B-B9BB24C4D521',
                text: 'Blue painters tape and tacks to hang posters.',
                checked: false
            },
            {
                id: '5FACAD71-E50F-4CB5-B679-5BE9E75CB2D2',
                text: 'Thank you notes',
                checked: false
            },
            {
                id: '90D85B73-94FC-4CFF-93D4-1456FED6BE1F',
                text: 'Backpack',
                checked: false
            },
            {
                id: 'B69FD6D1-E6E6-4E3B-8E5E-06498E18205B',
                text: 'Dry Erase Markers (Lots)',
                checked: false
            },
            {
                id: '948ADA57-8CA8-4FCB-86B4-1740EE2ECE4A',
                text: 'Calculator / Mini Stapler',
                checked: false
            },
                                {
                                    id: 'AE293AD3-A2F5-42C2-9C0D-CFEB90193B59',
                                    text: 'Notebooks / Folders / Post-Its (you will love graphing paper notebooks)',
                                    checked: false
                                },
                                    {
                                        id: '11436846-13A0-4756-AF26-5EDF6DA12D53',
                                        text: 'Pens / Pencils / Highlighters / Eraser',
                                        checked: false
                                    },
                                    {
                                        id: 'F9735297-D9C1-4554-A89F-86109A360285',
                                        text: 'Computer / Charger',
                                        checked: false
                                    },
                                        {
                                            id: 'A5E1222B-90EC-47AE-B5BF-73053033B54F',
                                            text: 'Shower basket – A simple one is the best. When they start adding a lot of compartments that just means space for mold. Go simple!',
                                            checked: false
                                        },
                                        {
                                            id: 'C236D609-F561-4511-BF9E-A7324C544556',
                                            text: 'Shower Shoes, towels (big towels are great for coverage)',
                                            checked: false
                                        },
                                            {
                                                id: '0AAE6394-8003-401A-8A9D-8DCE61D36485',
                                                text: 'Close toed shoes, sandals, sneakers, dress shoes.',
                                                checked: false
                                            },
                                            {
                                                id: '401E7903-72D5-41FD-8D9D-6984AABE03BC',
                                                text: '1 Interview Outfit for career day (Make sure they fit before packing)',
                                                checked: false
                                            },
                                                {
                                                    id: 'DC6818E7-37DC-4585-A945-208DD90BDE10',
                                                    text: 'Activity Clothing – For climbing south table and going to the gym.',
                                                    checked: false
                                                },
                                                {
                                                    id: '63CA1042-C281-4BFC-9616-124EDC1FD0FE',
                                                    text: 'Sunblock, face cream w/ SPF, hat and sunglasses',
                                                    checked: false
                                                },
                                                    {
                                                        id: '7025F331-CF04-44A2-9320-00C71AD22597',
                                                        text: 'Lotion – it is really dry here! We are all fans of the Vaseline w/ Aloe as it doubles for sunburn relief',
                                                        checked: false
                                                    },
                                                    {
                                                        id: 'F6EFCF5C-F98E-409A-84F1-311E3CC35D01',
                                                        text: 'Hair products, Makeup and Brushes',
                                                        checked: false
                                                    },
                                                        {
                                                            id: 'A28D9524-085F-4471-AE85-C7926349D295',
                                                            text: 'Shampoo / Conditioner / body wash / deodorant – Studying is not an excuse for smelling bad.',
                                                            checked: false
                                                        },
                                                        {
                                                            id: '08012515-A31F-47A5-8C6E-656CE1C136E0',
                                                            text: 'School Clothing – You can’t wear pajamas all day every day',
                                                            checked: false
                                                        },
                                                            {
                                                                id: '87728ad7-bbaf-47fc-92b3-a874420e4825',
                                                                text: 'Roku, Apple TV (3G or higher), or a laptop (with HDMI out)',
                                                                checked: false
                                                            }
            ];

            function saveChecklist () {
                localStorage[RSL_LOCALSTORAGE_KEY] = JSON.stringify($scope.checklist, function (key, val) {
                    if (key == '$$hashKey') {
                        return undefined;
                    }
                    return val;
                });
            };

            $scope.loadChecklist = function () {
                if (localStorage[RSL_LOCALSTORAGE_KEY]) {
                    $scope.checklist = JSON.parse(localStorage[RSL_LOCALSTORAGE_KEY]);
                } else {
                    $scope.checklist = starterChecklist;
                }

            };

            $scope.toggleCheck = function (id) {
                for (var i in $scope.checklist) {
                    if ($scope.checklist[i].id === id) {
                        $scope.checklist[i].checked = !$scope.checklist[i].checked;
                    }
                }
                saveChecklist();
            };

            $scope.getChecked = function(id) {
                // var checked = 'unchecked';
                for (var i in $scope.checklist) {
                    if ($scope.checklist[i].id === id) {
                        if ($scope.checklist[i].checked) {
                            return 'checked';
                        }
                    }
                }
                return 'unchecked';
            };

            $scope.loadChecklist();

        }]);
