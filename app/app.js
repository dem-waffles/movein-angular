'use strict';

angular.module('services', []);
angular.module('controllers', ['services']);
angular.module('MoveIn', [
	'controllers',
	'ngRoute'
]);

angular.module('MoveIn')
	.config(['$routeProvider',
		function ($routeProvider) {
			$routeProvider.when('/', {
				redirectTo: '/home'
			})
			.when('/home', {
				templateUrl: '/app/components/home/home.html'
			})
			.when('/checklist', {
				templateUrl: '/app/components/checklist/checklist.html'
			})
			.when('/tlc-is-select', {
				templateUrl: '/app/components/select/tlc-is-select.html'
			})
			.when('/tlc-is', {
				templateUrl: '/app/components/directions/tlc-is.html',
                controller: 'DirectionsCtrl'
			})
			.when('/no-tlc-is', {
				templateUrl: '/app/components/directions/no-tlc-is.html',
                controller: 'DirectionsCtrl'
			})
			.otherwise({
				redirectTo: '/home'
			});
		}]);
